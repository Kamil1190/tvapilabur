﻿using System.Collections.Generic;
using System.Linq;
using TvApiLabUr.DAL;
using TvApiLabUr.Models;

namespace TvApiLabUr.Services
{
    public class MoviesService
    {
        public List<MovieResponse> GetAll()
        {
            using (var ctx = new MoviesContext())
            {
                return ctx.Movies.Select(m => new MovieResponse()
                {
                    Id = m.Id,
                    Title = m.Title,
                    Year = m.Year,
                    AverageRate = m.Reviews.Any() ? m.Reviews.Average(r => r.Rate) : 0
                }).ToList();
            }
        }

        public MovieResponse GetById(int id)
        {
            using (var ctx = new MoviesContext())
            {
                var movie = ctx.Movies.Find(id);
                if (movie == null)
                {
                    return null;
                }

                return new MovieResponse()
                {
                    Id = movie.Id,
                    Title = movie.Title,
                    Year = movie.Year,
                    AverageRate = movie.Reviews.Any() ? movie.Reviews.Average(r => r.Rate) : 0
                };
            }
        }

        public void AddNewMovie(MovieRequest movie)
        {
            using (var ctx = new MoviesContext())
            {
                ctx.Movies.Add(new Movie()
                {
                    Title = movie.Title,
                    Year = movie.Year
                });
                ctx.SaveChanges();
            }
        }
        
        public void Remove(int id)
        {
            using (var ctx = new MoviesContext())
            {
                var movie = ctx.Movies.Find(id);
                if (movie == null)
                {
                    return;
                }

                ctx.Movies.Remove(movie);
                ctx.SaveChanges();
            }
        }

        public List<MovieResponse> GetAllByYear(int year)
        {
            using (var ctx = new MoviesContext())
            {
                return ctx.Movies.Where(m => m.Year == year).Select(m => new MovieResponse()
                {
                    Id = m.Id,
                    Title = m.Title,
                    Year = m.Year,
                    AverageRate = m.Reviews.Any() ? m.Reviews.Average(r => r.Rate) : 0
                }).ToList();
            }
        }

        public List<MovieResponse> GetAllByTitle(string title)
        {
            using (var ctx = new MoviesContext())
            {
                return ctx.Movies.Where(m => m.Title.Contains(title)).Select(m => new MovieResponse()
                {
                    Id = m.Id,
                    Title = m.Title,
                    Year = m.Year,
                    AverageRate = m.Reviews.Any() ? m.Reviews.Average(r => r.Rate) : 0
                }).ToList();
            }
        }

    }
}