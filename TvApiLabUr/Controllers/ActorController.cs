﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using TvApiLabUr.Models;
using TvApiLabUr.Services;

namespace TvApiLabUr.Controllers
{
    public class ActorController : ApiController
    {
        private ActorsService _actorsService;

        public ActorController()
        {
            _actorsService = new ActorsService();
        }

        [HttpGet, Route("actors")]
        public IHttpActionResult GetAllActors()
        {
            return Ok(_actorsService.GetAllActors());
        }

        [HttpPost, Route("actors")]
        public IHttpActionResult AddActor([FromBody] ActorRequest actor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            _actorsService.AddActor(actor);
            return Ok();
        }


        [HttpPost, Route("movies/actors")]
        public IHttpActionResult AttachActorToMovie([FromBody] JObject data)    //can't pass multi parameters in post?
        {
            dynamic json = data;
            int? actorId = json.actorId;
            int? movieId = json.movieId;
            if (movieId != null && actorId != null)
            {
                if (_actorsService.AttachActorToMovie(movieId.Value, actorId.Value))
                {
                    return Ok();
                }
            }
            return BadRequest();
        }
    }
}
