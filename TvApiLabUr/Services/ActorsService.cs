﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TvApiLabUr.DAL;
using TvApiLabUr.Models;

namespace TvApiLabUr.Services
{
    public class ActorsService
    {
        public void AddActor(ActorRequest request)
        {
            using (var ctx = new MoviesContext())
            {
                ctx.Actors.Add(new Actor()
                {
                    FirstName = request.FirstName,
                    LastName = request.LastName
                });
                ctx.SaveChanges();
            }
        }

        public bool AttachActorToMovie(int movieId, int actorId)
        {
            using (var ctx = new MoviesContext())
            {
                var movie = ctx.Movies.Find(movieId);
                var actor = ctx.Actors.Find(actorId);
                if (movie != null && actor != null)
                {
                    movie.Actors.Add(actor);
                    ctx.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public IEnumerable<ActorRequest> GetActorsForMovie(int movieId)
        {
            using (var ctx = new MoviesContext())
            {
                var movie = ctx.Movies.Find(movieId);
                return movie.Actors.Select(x => new ActorRequest()
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();
            }
        }

        public IEnumerable<ActorResponse> GetAllActors()
        {
            using (var ctx = new MoviesContext())
            {
                return ctx.Actors.Select(x => new ActorResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();
            }
        }
    }
}