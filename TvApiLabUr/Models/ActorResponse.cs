﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TvApiLabUr.Models
{
    public class ActorResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}