﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TvApiLabUr.DAL;

namespace TvApiLabUr.Models
{
    public class ActorRequest
    {
        [Required]
        [RegularExpression("^[a-zA-Z]*$")]
        public string FirstName { get; set; }
        [Required]
        [RegularExpression("^[a-zA-Z]*$")]
        public string LastName { get; set; }
    }
}