using System.Collections.Generic;
using TvApiLabUr.DAL;

namespace TvApiLabUr.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TvApiLabUr.DAL.MoviesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TvApiLabUr.DAL.MoviesContext context)
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie()
                {
                    Title = "Logan",
                    Year = 2017,
                    Actors = new List<Actor>()
                    {
                        new Actor()
                        {
                            FirstName = "Hugh",
                            LastName = "Jackman",
                        },
                        new Actor()
                        {
                            FirstName = "Patric",
                            LastName = "Stewart",
                        }
                    },
                    Reviews = new List<DAL.Review>()
                    {
                        new DAL.Review()
                        {
                            Comment = "awesome",
                            Rate = 9
                        },
                        new DAL.Review()
                        {
                            Comment = "best movie",
                            Rate = 10
                        },
                        new DAL.Review()
                        {
                            Comment = "good",
                            Rate = 5
                        },
                        new DAL.Review()
                        {
                            Comment = "not for me",
                            Rate = 2
                        },
                        new DAL.Review()
                        {
                            Comment = "ok",
                            Rate = 6
                        },
                        new DAL.Review()
                        {
                            Comment = "awesome",
                            Rate = 10
                        }
                    }

                },
                new Movie()
                {
                    Title = "Beauty and the Beast",
                    Year = 2017,
                    Actors = new List<Actor>()
                    {
                        new Actor()
                        {
                            FirstName = "Emma",
                            LastName = "Watson",
                        },
                        new Actor()
                        {
                            FirstName = "Dan",
                            LastName = "Stevens",
                        }
                    },
                    Reviews = new List<DAL.Review>()
                    {
                        new DAL.Review()
                        {
                            Comment = "awesome",
                            Rate = 8
                        },
                        new DAL.Review()
                        {
                            Comment = "best movie",
                            Rate = 9
                        }
                    }
                },
                new Movie()
                {
                    Title = "Bogowie",
                    Year = 2014,
                    Actors = new List<Actor>()
                    {
                        new Actor()
                        {
                            FirstName = "Tomasz",
                            LastName = "Kot",
                        },
                        new Actor()
                        {
                            FirstName = "Piotr",
                            LastName = "G�owacki",
                        }
                    }
                },
                new Movie()
                {
                    Title = "Whiplash",
                    Year = 2014,
                    Actors = new List<Actor>()
                    {
                        new Actor()
                        {
                            FirstName = "Miles",
                            LastName = "Teller",
                        },
                        new Actor()
                        {
                            FirstName = "Paul",
                            LastName = "Reiser",
                        }
                    }
                }
            };

            context.Movies.AddRange(movies);
            context.SaveChanges();
        }
    }
}
